
"""    
    Copyright (C) 2017-2019 Manon Michel <manon.michel@uca.fr>

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

"""

import random, math, numpy, scipy.stats,sys
import argparse, time



def Parser(parser):
        parser.add_argument("-N_DIM", default=100, type = int, dest = 'N_DIM',
                            help='Integer indicating'
                            +' the dimension of the target distribution. Default = 100')
        parser.add_argument("-VARIANCE_ARG", default=1, type = float, 
                            dest = 'VARIANCE_ARG',
                            help='Gaussian Prior Variance argument. Default = 1')
        parser.add_argument("-SEED_THETA", default=1, type = int, dest = 'SEED_THETA',
                            help = 'Integer indicating '
                            +'the seed to use to generate the Theta rates. Default = 1')
        parser.add_argument("-SEED_Y", default=1, type = int, dest = 'SEED_Y',
                            help = 'Integer indicating '
                            +'the seed to use to generate the data. Default = 1')                                   
        parser.add_argument("-STORE", default='.', type = str, dest = 'STORE',
                            help='Name of folder for storing data. Default = "."') 
        parser.add_argument('-INIT_EQ', action='store_true', dest = 'INIT_EQ',
                             help='Specified if initialization from prior')  

def declare_var(self, dic_args):
    self.N_DIM = dic_args['N_DIM']  
    if int(self.N_DIM**0.5) != self.N_DIM ** 0.5: 
        print "Error: N_DIM is not a square. Aborting."
        sys.exit()
    self.VARIANCE_ARG = dic_args['VARIANCE_ARG']
    self.SEED_THETA = dic_args['SEED_THETA']
    self.SEED_Y = dic_args['SEED_Y']
    self.name_class = self.__class__.__name__
    self.extra_arg=True
    self.FakeEv = 0
    self.d = 0.0   
    self.INIT_EQ = dic_args['INIT_EQ']

def get_rootfile(dic_args, name_class, arg=None):
    """ Set filename for data """
    if arg is None:
        folder = dic_args['STORE']
    else:
        folder = arg+dic_args['STORE']
    dictionary_rootfile = {'RootNameForFile' :folder+'/LibraryPoissonGaussModel_'+name_class+'_Dim%i_VarianceArg%f_SeedTheta%i_SeedY%i_'
                               %(dic_args['N_DIM'], dic_args['VARIANCE_ARG'],dic_args['SEED_THETA'],dic_args['SEED_Y'])}
    return dictionary_rootfile         

class GaussianPrior:
    def __init__(self, dic_args): 
        """Inits Cox Model with Gaussian prior with distribution parameters value (default values
           or defined via the parser"""
        declare_var(self, dic_args)
        self.N_DIM_SQRT = self.N_DIM ** 0.5
        self.init_covar()
        self.init_data()

    def init_covar(self):
        self.VARIANCE_ARRAY = numpy.array([[numpy.exp(- 1.0 / (2.0 * self.VARIANCE_ARG) 
                                              * numpy.sqrt(
                                                (i % self.N_DIM_SQRT - j % self.N_DIM_SQRT) ** 2.0 
                                              + (i // self.N_DIM_SQRT-j // self.N_DIM_SQRT) ** 2.0
                                                           ))
                                            for i in range(self.N_DIM)]
                                            for j in range(self.N_DIM)
                                            ])
        self.VARIANCEINV_ARRAY = numpy.linalg.inv(self.VARIANCE_ARRAY)

    def init_data(self):
        """Inits data """
        numpy.random.seed(self.SEED_THETA)
        self.THETA_TH = numpy.random.randn(self.N_DIM)      
        numpy.random.seed(self.SEED_Y)  
        self.Y = numpy.random.poisson(numpy.exp(self.THETA_TH))
        numpy.random.seed(int(time.time()))     

    def init_config(self):
        """Inits position of sampler """
        x = numpy.zeros(self.N_DIM)
        if self.INIT_EQ:
            x = numpy.random.randn(self.N_DIM)
        return x

    def initEC(self, scheme_arg=None):
        """Inits sampler """
        self.FakeEv = 0
        self.d = 0.0   
        if scheme_arg not in ['BOUNCE', 'FORWARD', None]:
            "Error. Class does not support yet this scheme."
        else:
            lift_vector = numpy.random.normal(0.0, 1.0, self.N_DIM)
            lift_vector /= numpy.dot(lift_vector,lift_vector) ** 0.5
        return lift_vector       

    def do_lift(self, old_x, distance, update_vector):
        """ Update position along update_vector """
        return old_x + distance * update_vector 
        
    def E(self, x):
        """Computes Energy function at x"""
        return 0.5 * numpy.dot(numpy.dot(self.VARIANCEINV_ARRAY, x), x) - numpy.dot(self.Y, x)  + numpy.sum(numpy.exp(x))         
        
    def E_gauss(self, x):
        """Computes Energy function at x - Gaussian component"""
        return 0.5 * numpy.dot(numpy.dot(self.VARIANCEINV_ARRAY, x), x) 

    def norm(self, x):
        """Computes norm function at x"""
        return sum(x**2.0)
        
    def dE_gauss_vec(self, x):
        """Computes  gradient of the Gaussian likelihood part at x """
        return numpy.dot(self.VARIANCEINV_ARRAY, x)

    def dE_lin_vec(self, x):
        """ Computes gradient of the linear likelihood part at x """
        return -self.Y

    def dE_exp_vec(self, x):
        """ Computes gradient of the exp likelihood part at x """
        return numpy.exp(x)
        
    def dE_poly_vec(self, x):
        """Computes  gradient of the Gaussian and linear likelihood parts at x """
        return numpy.dot(self.VARIANCEINV_ARRAY, x) - self.Y

    def dE_vec(self, x):
        """ Computes gradient  at x """
        return numpy.dot(self.VARIANCEINV_ARRAY, x)  - self.Y  + numpy.exp(x)
         
    def rate_function(self, x, update_vector):
        """ Compute the rate function """
        return max(0, numpy.dot(self.dE_vec(x), update_vector))

    def rate_function_bound(self, x, update_vector):
        """ Compute the rate function of the bounding PP """
        a_gauss_lin = max(0, numpy.dot(self.dE_poly_vec(x), update_vector))
        a_exp = numpy.sum(numpy.maximum(0, self.dE_exp_vec(x) * update_vector))
        return a_gauss_lin + a_exp 

    def deltaE_Metro(self, old_x, new_x):
        """ Return Difference of potential between two configurations """
        return self.E(new_x) - self.E(old_x)
        
    def propose_move(self, x, delta):
        """ Propose a configuration for Metropolis-type algorithms """
        update_vector = numpy.random.normal(0.0, 1.0, self.N_DIM)
        update_vector /= numpy.dot(update_vector, update_vector) ** 0.5
        return x.copy() + random.uniform(0.0, 1.0) * delta * update_vector

    def get_next_event_poly(self, x, update_vector):
        """ Compute event for the polynomial part. Equation type a(lstop-lmin)**2 + Emin = E(lstop) = deltaE"""
        distance_to_next_event = float("inf")
        DeltaEStar = - math.log(random.uniform(0.0, 1.0)) 
        a = self.E_gauss(update_vector)
        initial_min_dist = numpy.dot(update_vector, self.dE_gauss_vec(x)-self.Y)/ (2.0 * a)
        E0 = self.E_gauss(x) - numpy.dot(self.Y, x)
        Emin = E0 - a * initial_min_dist ** 2.0
        if initial_min_dist > 0.0:
            DeltaEStar += E0
        else:
            DeltaEStar += Emin
        distance_to_next_event = - initial_min_dist + ((DeltaEStar - Emin) / a) ** 0.5 
        return distance_to_next_event

    def get_next_event(self, x, update_vector):
        """ Compute the next event for PDMC-type algorithm (based on Thinning) """
        distance_to_next_event = 0.0
        x_running = x.copy()
        while True:
            self.FakeEv += 1                           
            tau =  self.get_next_event_poly(x_running, update_vector)                                                  
            for i in range(self.N_DIM):
                if update_vector[i] > 0.0:
                    tau_exp_i = numpy.log(1.0-numpy.log(numpy.random.rand())/numpy.exp(x_running[i])) / update_vector[i]
                    tau = min(tau, tau_exp_i)     
            distance_to_next_event += tau    
            x_running += tau * update_vector                       
            Thinning_rate = self.rate_function(x_running, update_vector) / self.rate_function_bound(x_running, update_vector)
            assert Thinning_rate < 1.001, 'Thinning=%f. Bound < True ! Stopping.'%Thinning_rate
            if Thinning_rate != 0:
                if numpy.random.rand() <= Thinning_rate:
                    break     
        self.d += distance_to_next_event   
        return distance_to_next_event

