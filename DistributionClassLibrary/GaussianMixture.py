"""    
    Copyright (C) 2017-2019 Manon Michel <manon.michel@uca.fr>

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

"""

##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~###
import random, math, numpy, scipy.stats
import argparse, sys
import time
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~###

def Parser(parser):
        parser.add_argument("-N_DIM", default=2, type = int, dest = 'N_DIM',
                            help='Dimension of target distribution. Default=2')
        parser.add_argument("-SEED", default=1, type = int, dest = 'SEED',
                            help = 'Random seed. Default = 1')
        parser.add_argument("-N_GAUSS", default=5, type = int, dest = 'N_GAUSS',
                            help = 'Number of distributions in mixture. Default=5')
        parser.add_argument("-STORE", default='.', type = str, dest = 'STORE',
                            help='Name of folder for storing data. Default ="."') 
        parser.add_argument('-INIT_EQ', action='store_true', dest = 'INIT_EQ',
                             help='If specified, initialization from target distribution')  

def get_rootfile(dic_args, name_class, arg=None):
    if arg is None:
        folder = dic_args['STORE']
    else:
         folder = arg+dic_args['STORE'] 
    dictionary_rootfile = {'RootNameForFile' :folder+'/LibraryMixtureGaussian_'+name_class+'NGauss'+str(dic_args['N_GAUSS'] )+'_Dim'+str(dic_args['N_DIM'] )+'_Seed%i'%(dic_args['SEED'])+'_'}
    return dictionary_rootfile         

def declare_var(self, dic_args):
    self.N_DIM = dic_args['N_DIM']
    self.N_GAUSS = dic_args['N_GAUSS']
    self.INIT_EQ = dic_args['INIT_EQ']
    self.name_class = self.__class__.__name__ 
    if 'Ran' in self.name_class:
            self.SEED = dic_args['SEED'] 

class RanNGaussianND:
    def __init__(self, dic_args): 
        """Inits with distribution parameters value (default values
           or defined via the parser"""
        self.name_class = self.__class__.__name__ 
        declare_var(self, dic_args)
        #-------------------------------#
        numpy.random.seed(self.SEED)
        varlist =  numpy.random.uniform(low=0.5,high=3.0,size=self.N_DIM)
        self.VARIANCE_LIST = numpy.array([numpy.random.permutation(varlist) for g in range(self.N_GAUSS)
                                         ])
        self.MEAN_LIST =  [numpy.zeros(self.N_DIM)
                                          ]
        for g in range(1,self.N_GAUSS):
                ran1 = numpy.random.uniform(low=1.0,high=2.0,size=self.N_DIM)
                ran2 = numpy.random.uniform(low=1.0,high=2.0,size=self.N_DIM)
                self.MEAN_LIST += [self.MEAN_LIST[-1] +  (ran1*self.VARIANCE_LIST[g] ** 0.5 +  ran2*self.VARIANCE_LIST[g-1] ** 0.5)]
                self.MEAN_LIST[-1] = numpy.array(self.MEAN_LIST[-1])
        self.MEAN_LIST = numpy.array(self.MEAN_LIST)
        self.p_list =  numpy.ones(self.N_GAUSS)
        numpy.random.seed(int(time.time()))
        #---------------------------------#
        self.p_list /= sum(self.p_list)
        self.norm_list = numpy.array([numpy.prod(1.0 / (2.0 * math.pi* v)**0.5) for v in self.VARIANCE_LIST])
        self.VARIANCEINV_LIST = numpy.array([v**-1 for v in self.VARIANCE_LIST])
        self.p_list_norm = self.p_list * self.norm_list

    def init_config(self):
        """Inits position of sampler """
        if not self.INIT_EQ:
            x = numpy.random.rand(self.N_DIM)
        else:
            print 'INIT EQUILIBRATED'
            nu = random.uniform(0.0,1.0)    
            gauss_index = 0
            while nu > self.p_list[gauss_index]:
                    gauss_index += 1
                    nu -=  self.p_list[gauss_index]
            x = numpy.random.normal(self.MEAN_LIST[gauss_index], self.VARIANCE_LIST[gauss_index]**0.5)
        return x
            
    def initEC(self, scheme_arg=None):
        """Inits sampler """
        if scheme_arg is None:
            lift_vector = numpy.random.normal(0.0, 1.0, self.N_DIM)
            lift_vector /= numpy.dot(lift_vector,lift_vector) ** 0.5
        if scheme_arg == 'BOUNCE' or scheme_arg == 'FORWARD':
            lift_vector = numpy.random.normal(0.0, 1.0, self.N_DIM)
            lift_vector /= numpy.dot(lift_vector,lift_vector) ** 0.5
        elif scheme_arg == 'STRAIGHT':
            lift_vector = numpy.zeros(self.N_DIM)
            lift_vector[int(random.uniform(0.0, 1.0) * self.N_DIM)] = random.choice([-1.0, 1.0])
        return lift_vector
            
    def do_lift(self, old_x, distance, update_vector):
        """ Update position along update_vector """
        return old_x + distance * update_vector 
        
    def norm(self, x):
        """Computes norm function at x"""
        return sum(x**2.0)
        
    def dE(self, x):      
        """Computes summed Energy function gradient at x"""   
        return numpy.dot(self.VARIANCEINV_LIST, x)

    def E(self, x):
        """Computes Energy function at x"""
        return -math.log(self.proba_config(x))

    def proba_config(self,x):
        """Computes configuration probability"""
        return sum(self.p_list * numpy.exp(- 0.5 * sum((self.VARIANCEINV_LIST * (x - self.MEAN_LIST)**2.0).T)))

    def dE_vec(self, x):      
        """Computes Energy function gradient at x"""  
        weight = self.p_list * numpy.exp(- 0.5 * sum((self.VARIANCEINV_LIST * (x - self.MEAN_LIST)**2.0).T))
        weight /= sum(weight)
        return numpy.dot((self.VARIANCEINV_LIST *  (x- self.MEAN_LIST)).T, weight)

    def dE_vec_factor(self, x, i):      
        """Computes Energy function gradient at x"""
        weight = self.p_list * numpy.exp(- 0.5 * sum((self.VARIANCEINV_LIST * (x - self.MEAN_LIST)**2.0).T))
        weight /= sum(weight)
        return weight * self.VARIANCEINV_LIST[i] *  (x- self.MEAN_LIST[i])

    def deltaE_Metro(self, old_x, new_x):
        """ Computes difference in energy or minus log of probability """
        return self.E(new_x) - self.E(old_x)
        
    def propose_move(self, x, delta):
        """ Propose a discrete update of the position """
        update_vector = numpy.random.normal(0.0, 1.0, self.N_DIM)
        update_vector /= numpy.dot(update_vector, update_vector) ** 0.5
        return x.copy() + random.uniform(0.0,1.0) * delta * update_vector
               
    def get_clock_event(self, x, update_vector, distance, factor):
        """ Computes Poisson process thinning"""
        distance += self.get_next_event_gauss(x + distance * update_vector, update_vector, self.VARIANCEINV_LIST[factor], self.MEAN_LIST[factor])
        weight = self.p_list * numpy.exp(- 0.5 * sum((self.VARIANCEINV_LIST * (x+ update_vector * distance  - self.MEAN_LIST)**2.0).T))
        if weight[factor] == 0.0:
                return distance, 0.0
        else:
                ratio_N = weight / sum(weight)
                grad =numpy.dot(self.VARIANCEINV_LIST * (x + update_vector * distance - self.MEAN_LIST), update_vector)
                ratio = max(0, sum(grad * ratio_N)) / sum(max(0, grad[j]) for j in range(self.N_GAUSS))
                return distance, ratio

    def get_next_event_gauss(self, x, update_vector, var_inv, mean):
        """ Computes event for PDMC for a Gaussian distribution"""
        DeltaEStar = - math.log(random.uniform(0.0, 1.0))           
        a = 0.5 * numpy.dot(var_inv, update_vector ** 2.0)
        initial_min_dist = numpy.dot(update_vector, var_inv * (x-mean))/ (2.0 * a)
        E0 = 0.5 * numpy.dot(var_inv,(x - mean) ** 2.0)
        Emin = E0 - a * initial_min_dist ** 2.0
        if initial_min_dist > 0.0:
            DeltaEStar += E0
        else:
            DeltaEStar += Emin
        distanceToNextEvent = - initial_min_dist + ((DeltaEStar - Emin) / a) ** 0.5 
        return distanceToNextEvent

    def get_next_event(self, x, update_vector):
        """ Computes event for PDMC using thinning"""
        NEval = 0.0
        distanceToNextEvent = numpy.zeros(self.N_GAUSS) 
        dmin = -1.0
        list_factor = range(self.N_GAUSS)
        while len(list_factor) != 0:
            for factor in list_factor:
                if dmin>0.0 and distanceToNextEvent[factor] > dmin:
                    list_factor.pop(list_factor.index(factor))
                else:        
                    distanceToNextEvent[factor], ratio = self.get_clock_event(x, update_vector,distanceToNextEvent[factor], factor) 
                    NEval += 1
                    if random.uniform(0.0, 1.0) < ratio:
                        list_factor.pop(list_factor.index(factor))
                        if dmin > 0.0:
                            dmin = min(distanceToNextEvent[factor], dmin)
                        else:
                            dmin = distanceToNextEvent[factor]
        return dmin

    def get_Mean(self, obs, arg_obs):
        """ Get theoretical mean"""
        if obs == 'x':
            if arg_obs is None:
                    return numpy.dot(self.p_list, self.MEAN_LIST)
            else:
                return  numpy.dot(self.p_list, self.MEAN_LIST)[int(arg_obs)]
        elif obs=='norm':
            return sum(numpy.dot(self.p_list, self.VARIANCE_LIST+ self.MEAN_LIST**2.0))
        if 'cov' in obs:
            return 0.0 
        else:
            print 'MEAN NOT DEFINED';sys.exit()
        

    def get_Var(self, obs, arg_obs):
        """ Get theoretical variance"""
        if obs == 'x':
            var_x = numpy.dot(self.p_list,(self.VARIANCE_LIST+ self.MEAN_LIST**2.0))
            var_x -= numpy.dot(self.p_list,self.MEAN_LIST)**2.0
            if arg_obs is None:
                return sum(var_x)
            else:
                return var_x[int(arg_obs)]
        elif obs=='norm': 
            mean_single = sum((self.VARIANCE_LIST+ self.MEAN_LIST**2.0).T)
            mean_norm = numpy.dot(self.p_list, mean_single)
            var_norm = numpy.dot(self.p_list, sum((self.VARIANCE_LIST ** 2.0 * 2.0 + 4.0 * self.MEAN_LIST ** 2.0 * self.VARIANCE_LIST).T) + mean_single ** 2.0)
            var_norm -= mean_norm ** 2.
            return var_norm   
        else:
            print 'VAR NOT DEFINED';sys.exit()
            
    def get_hist(self, bins, x1):
        """ Get theoretical histogram"""
        p_norm_1d = self.p_list * numpy.array([1.0 / (2.0 * math.pi* v[x1])**0.5 for v in self.VARIANCE_LIST] )
        th =sum(p_norm_1d[i]*numpy.exp(-(bins-self.MEAN_LIST[i][x1])**2.0*0.5/self.VARIANCE_LIST[i][x1]) for i in range(self.N_GAUSS))
        th /= sum(th * (bins[1]-bins[0]))
        return th
