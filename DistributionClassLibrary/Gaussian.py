"""    
    Copyright (C) 2017-2019 Manon Michel <manon.michel@uca.fr>

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

"""

##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~###
import random, math, numpy, scipy.stats, os
import argparse, time
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~###


def Parser(parser):
        parser.add_argument("-N_DIM", default=100, type = int, dest = 'N_DIM',
                            help=' Integer indicating the dimension of the target distribution. Default=100')
        parser.add_argument("-VARIANCE_ARG", default=6, type = int, dest = 'VARIANCE_ARG',
                            help='Maximal eigenvalue'
                            +'of the covariance matrix of the Gaussian distribution'
                            +'(log-linearly distributed between 1 and 10^Variance_max). Default=6')
        parser.add_argument("-SEED", default=1, type = int, dest = 'SEED',
                            help = 'Integer indicating'
                            +'the seed to use to generate the covariance matrix. Default=1')
        parser.add_argument('-FACTOR', action='store_true', dest = 'FACTOR',
                            help="If specified, compute events with factorized rates for PDMC-type algorithms")  
        parser.add_argument("-STORE", default='.', type = str, dest = 'STORE',
                            help="Name of folder for data storing. Default='.'") 
        parser.add_argument('-INIT_EQ', action='store_true', dest = 'INIT_EQ',
                             help="If specified, initialize configuration from target distribution.")  
        parser.add_argument('-ILL', action='store_true', dest = 'ILL',
                             help="If specified, set a random basis for update vectors in ZZ-type algorithms.")  

def declare_var(self, dic_args):
    self.N_DIM = dic_args['N_DIM']  
    self.VARIANCE_ARG = dic_args['VARIANCE_ARG']
    self.name_class = self.__class__.__name__
    self.FACTOR = dic_args['FACTOR']
    self.ILL = dic_args['ILL']
    self.SEED = dic_args['SEED']
    self.INIT_EQ = dic_args['INIT_EQ']

def get_rootfile(dic_args, name_class, arg=None):
    """ Set filename for data """
    if arg is None:
        folder = dic_args['STORE']
    else:
         folder = arg+dic_args['STORE'] 
    dictionary_rootfile = {'RootNameForFile' :folder+'/LibraryGaussian_'+name_class+'_Dim%i_VarianceArg%f_'
                               %(dic_args['N_DIM'], dic_args['VARIANCE_ARG'])}
    if dic_args['ILL']:
        dictionary_rootfile['RootNameForFile'] += 'Seed%i_'%(dic_args['SEED'])
    return dictionary_rootfile         

class GaussianLogLinearVar:
    def __init__(self, dic_args): 
        """Inits GaussianLogLinearVar with distribution parameters value (default values
           or defined via the parser"""
        declare_var(self, dic_args)
        self.VARIANCE_LIST = 10 ** numpy.linspace(0.0, self.VARIANCE_ARG, self.N_DIM)
        self.VARIANCEINV_LIST = self.VARIANCE_LIST ** -1
        self.output_distance_array = self.VARIANCE_LIST ** 0.5
        if not self.FACTOR:
                self.get_next_event = self.get_next_event_gen
                self.do_lift = self.do_lift_gen
        elif self.ILL:
                print 'Factor set to ill'
                self.get_next_event = self.get_next_event_factor_ill
                self.do_lift = self.do_lift_ill
        else:   
                self.get_next_event = self.get_next_event_factor
                self.do_lift = self.do_lift_gen
               
               
    def init_config(self):
        """Inits position of sampler """
        x = numpy.random.rand(self.N_DIM)
        if self.INIT_EQ:
            x=numpy.random.multivariate_normal(numpy.zeros(self.N_DIM), 
                                               numpy.diag(self.VARIANCE_LIST), 1)[0]
        return x

    def initEC(self, scheme_arg=None):
        """Inits sampler """
        if scheme_arg is None:
            lift_vector = numpy.random.normal(0.0, 1.0, self.N_DIM)
            lift_vector /= numpy.dot(lift_vector,lift_vector) ** 0.5
        elif scheme_arg == 'BOUNCE' or scheme_arg == 'FORWARD':
            lift_vector = numpy.random.normal(0.0, 1.0,  self.N_DIM)
            lift_vector /= numpy.dot(lift_vector,lift_vector) ** 0.5
        elif scheme_arg == 'STRAIGHT':
            lift_vector = 2.0*numpy.random.randint(2, size=self.N_DIM) - numpy.ones(self.N_DIM)
            lift_vector /= self.N_DIM ** 0.5
        elif scheme_arg == 'ILL_STRAIGHT':
            if not os.path.isfile('orth_mat_dim'+str(self.N_DIM)+'.npy'):
                numpy.random.seed(1)
                orthogonal_matrix=scipy.stats.ortho_group.rvs(dim=self.N_DIM)
                numpy.save('orth_mat_dim'+str(self.N_DIM),orthogonal_matrix)
                print 'saved matrix'
                numpy.random.seed(int(time.time()))
            else:
                orthogonal_matrix = numpy.load('orth_mat_dim'+str(self.N_DIM)+'.npy')
            lift_vector = orthogonal_matrix.T
	    lift_vector /= self.N_DIM ** 0.5
        return lift_vector       

    def init_StraightEC(self):
        """Inits Optimized EC sampler """
        output_distance_array = 2 * self.VARIANCE_LIST ** 0.5
        update_vector_array = numpy.identity(self.N_DIM)
        return output_distance_array, update_vector_array
            
    def do_lift_gen(self, old_x, distance, update_vector):
        """ Update position along update_vector """
        return old_x + distance * update_vector 

    def do_lift_ill(self, old_x, distance, update_vector):
        """ Update position along all update_vector """
        return old_x + sum(distance * update_vector)
        
    def E(self, x):
        """Computes Energy function at x"""
        return 0.5 * numpy.dot(self.VARIANCEINV_LIST, x ** 2.0)
        
    def norm(self, x):
        """Computes norm function at x"""
        return sum(x**2.0)
        
    def dE(self, x):      
        """Computes summed Energy function gradient at x"""   
        return numpy.dot(self.VARIANCEINV_LIST, x)
         
    def dE_vec(self, x):      
        """Computes Energy function gradient at x"""   
        return self.VARIANCEINV_LIST * x        
         
    def dE_vec_factor(self, x, i=None):      
        """Computes factor component Energy function gradient at x"""   
        if i is None:
                i = self.event_factor                
        return (self.VARIANCEINV_LIST * x)[i]

    def get_factor(self):                   
        return self.event_factor

    def deltaE_Metro(self, old_x, new_x):
        """ Computes difference in energy or minus log of probability """
        return self.E(new_x) - self.E(old_x)
        
    def propose_move(self, x, delta):
        """ Propose a discrete update of the position """
        update_vector = numpy.random.normal(0.0, 1.0, self.N_DIM)
        update_vector /= numpy.dot(update_vector, update_vector) ** 0.5
        return x.copy() + random.uniform(0.0,1.0) * delta * update_vector

    def get_next_event_unidimensional(self, x, dim, direction):
        """ Computes event for a 1-D Gaussian"""
        distance_to_next_event = float("inf")
        DeltaEStar = - math.log(random.uniform(0.0, 1.0)) 
        a = self.VARIANCEINV_LIST[dim] / 2.0
        initial_min_dist = self.VARIANCEINV_LIST[dim] * x[dim] * direction/ (2.0 * a)
        E0 = self.VARIANCEINV_LIST[dim] * x[dim] ** 2.0 / 2.0
        Emin = E0 - a * initial_min_dist ** 2.0
        if initial_min_dist > 0.0:
            DeltaEStar += E0
        else:
            DeltaEStar += Emin
        distance_to_next_event = - initial_min_dist + ((DeltaEStar - Emin) / a) ** 0.5 
        return distance_to_next_event
             
    def get_next_event_gen(self, x, update_vector):
        """ Computes event for PDMC in general setting"""
        distance_to_next_event = float("inf")
        DeltaEStar = - math.log(random.uniform(0.0, 1.0)) 
        a = self.E(update_vector)
        initial_min_dist = numpy.dot(update_vector, self.dE_vec(x))/ (2.0 * a)
        E0 = self.E(x)
        Emin = E0 - a * initial_min_dist ** 2.0
        if initial_min_dist > 0.0:
            DeltaEStar += E0
        else:
            DeltaEStar += Emin
        distance_to_next_event = - initial_min_dist + ((DeltaEStar - Emin) / a) ** 0.5 
        return distance_to_next_event
               
    def get_next_event_factor(self, x, update_vector):
        """ Computes next event for ZZ in factorized setting"""
        distance_to_next_event = float("inf")
        E0 = self.E(x)
        dE0 = self.dE_vec(x)
        a_vec = 0.5 * self.VARIANCEINV_LIST * update_vector ** 2.0
        initial_min_dist_vec = update_vector * dE0 
        for factor in range(self.N_DIM):
                DeltaEStar = - math.log(random.uniform(0.0, 1.0)) 
                a = a_vec[factor]
                initial_min_dist =  initial_min_dist_vec[factor]/ (2.0 * a)
                Emin = E0 - a * initial_min_dist ** 2.0
                if initial_min_dist > 0.0:
                        DeltaEStar += E0
                else:
                        DeltaEStar += Emin
                distance_factor = - initial_min_dist + ((DeltaEStar - Emin) / a) ** 0.5 
                if distance_factor < distance_to_next_event:
                        distance_to_next_event = distance_factor
                        self.event_factor = factor                        
        return distance_to_next_event              

    def get_next_event_factor_ill(self, x, update_array):
        """ Computes next event for ZZ in general setting"""
        distance_to_next_event = float("inf") 
        dE0 = self.dE_vec(x)
        a_vec = 0.5 * sum(update_array) * self.VARIANCEINV_LIST 
        for factor in range(self.N_DIM):
             update_vector=update_array[factor]
             DeltaEStar = - math.log(random.uniform(0.0, 1.0)) 
             a = numpy.dot(a_vec, update_vector)
             initial_min_dist = numpy.dot(dE0, update_vector) / (2.0 * a)
             if a < 0.0:
                     if initial_min_dist > 0.0: 
                             continue
                     else:
                             if - initial_min_dist ** 2.0 * a < DeltaEStar: 
                                     continue
                             else:
                                    distance_factor = - initial_min_dist - (DeltaEStar/a + initial_min_dist ** 2.0) ** 0.5
             elif a > 0.0:
                     if  initial_min_dist < 0.0:
                             distance_factor =  - initial_min_dist + (DeltaEStar/a) ** 0.5
                     else:
                             distance_factor = - initial_min_dist + (DeltaEStar/a + initial_min_dist ** 2.0) ** 0.5
             elif a == 0.0:
		     continue
             if distance_factor < distance_to_next_event:
                     distance_to_next_event = distance_factor
                     self.event_factor = factor
        return distance_to_next_event
   
    def iid_sample(self, Number_sample):
        """ Generates a collection of independent samples following a
        ill-conditioned Gaussian distribution"""
        data_iid =  numpy.random.multivariate_normal(numpy.zeros(self.N_DIM), 
                                                    numpy.diag(self.VARIANCE_LIST), Number_sample) 
        return data_iid

    def get_Mean(self, obs, arg_obs):
        """ Get theoretical mean"""
        if obs == 'E':
            return self.N_DIM/2.0
        elif obs == 'x':
            if arg_obs is None:
                return numpy.zeros(self.N_DIM)
            else:
                return 0.0
        elif obs=='norm':
            return sum(self.VARIANCE_LIST) 
        if 'cov' in obs:
            return 0.0 
        else:
            print 'MEAN NOT DEFINED';sys.exit()
        

    def get_Var(self, obs, arg_obs):
        """ Get theoretical variance"""
        if obs == 'E':
            return self.N_DIM/2.0
        elif obs == 'x':
            if arg_obs is None:
                return sum(self.VARIANCE_LIST) 
            else:
                return self.VARIANCE_LIST[int(arg_obs)]   
        elif obs=='norm':
            return sum(self.VARIANCE_LIST**2.0) * 2.0   
        if 'cov' in obs:
            if 'last' in obs:
                return self.VARIANCE_LIST[-1] * self.VARIANCE_LIST[-2]
        else:
            print 'VAR NOT DEFINED';sys.exit()
            
