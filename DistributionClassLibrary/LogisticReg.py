"""    
    Copyright (C) 2017-2019 Manon Michel <manon.michel@uca.fr>

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

"""

##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~###
import random, math, numpy, sys
import argparse, time
from scipy.special import expit
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~###


def Parser(parser):
        parser.add_argument("-STORE", default='.', type = str, dest = 'STORE',
                            help="Name of folder for storing data. Default = '.'") 
        parser.add_argument("-DATASET", default='GermanCredit', type = str, dest = 'DATASET',
                             help="Name of dataset to load. Default = 'GermanCredit'") 
        parser.add_argument("-VARIANCE_PRIOR", default=1000, type = float, dest = 'VARIANCE_PRIOR',
                            help="Set Gaussian prior variance. Default = 1000")    
        parser.add_argument('-INIT_EQ', action='store_true', dest = 'INIT_EQ',
                            help="If specified, initialization from prior")  

def declare_var(self, dic_args): 
    self.VARIANCE_PRIOR = dic_args['VARIANCE_PRIOR']
    self.name_class = self.__class__.__name__
    self.DATASET = dic_args['DATASET']
    self.INIT_EQ = dic_args['INIT_EQ']

def get_rootfile(dic_args, name_class,arg=None):
    """ Set filename for data """
    if arg is None:
        folder = dic_args['STORE']
    else:
        folder = arg+dic_args['STORE']
    dictionary_rootfile = {'RootNameForFile' :folder+'/LibraryLogisticReg_'+name_class+'_DataSet'+dic_args['DATASET']+'_'} 
    dictionary_rootfile['RootNameForFile']+='VariancePrior%0.2f'%dic_args['VARIANCE_PRIOR']+'_'     
    return dictionary_rootfile         

class GeneralLogRegWithBias:
    """ Gaussian prior """
    def __init__(self, dic_args): 
        """Inits  with distribution parameters value (default values
           or defined via the parser"""
        declare_var(self, dic_args)
        self.X = numpy.loadtxt('./DataForLogReg/X_'+self.DATASET+'_norm.txt')
        self.Y  = numpy.loadtxt('./DataForLogReg/Y_'+self.DATASET+'.txt')
        self.X = numpy.insert(self.X, 0, 1, axis=1) #Adding Bias
        self.N_DIM = len(self.X[0])
        self.N_DATA = len(self.X)
        self.FACTOR = True
        random.seed(time.time())
        numpy.random.seed(int(time.time()))  
        self.VARIANCE_LIST_PRIOR = numpy.ones(self.N_DIM)  * self.VARIANCE_PRIOR
        self.VARIANCEINV_LIST_PRIOR= self.VARIANCE_LIST_PRIOR.copy()**-1

    def logit(self, x):
        """ Robust logistic function """
        return expit(x)

    def L(self, w):
        """Computes likelihood at x"""  
        p = self.logit(numpy.dot(self.X,w))
        return numpy.prod(p**(self.Y)*(1.0-p)**(1.0- self.Y)) 

    def varw(self,w):
            return sum(w**2.0)

    def E_explicit(self,p,y):
        return (numpy.dot(y,numpy.log(p))+numpy.dot(1-y,numpy.log(1-p)))/len(y)

    def E(self, w):
        """Computes Energy function at w"""  
        p = self.logit(numpy.dot(self.X,w))
        return -sum(numpy.dot(self.Y,numpy.log(p))+numpy.dot(1.-self.Y,numpy.log(1-p)))
          
    def dE_vec(self, w):      
        """Computes Energy function gradient at w"""  
        return numpy.dot((self.logit(numpy.dot(self.X,w)) - self.Y), self.X)

    def dE_vec_factor_norm(self,w, i=None):
        """Computes normalized Energy function gradient at w for factor i"""  
        if i is None:
            i = self.event_factor
        if i<0.0:
                dE=self.VARIANCEINV_LIST_PRIOR*w
                dE/=numpy.linalg.norm(dE)
                return  dE
        else:
                if self.Y[i]==1:
                        dE=-self.X[i].copy()
                else:
                        dE=self.X[i].copy()
                dE /= numpy.linalg.norm(dE)        
                return dE
          
    def dE_vec_factor(self, w, i=None):      
        """Computes Energy function gradient at w for factor i (i =-1 corresponds to prior event)"""  
        if i is None:
            i = self.event_factor
        if i<0.0:
                return self.VARIANCEINV_LIST_PRIOR*w
        else:
                return numpy.dot((self.logit(numpy.dot(self.X[i],w)) - self.Y[i]), self.X[i])
         
    def dE(self, w):      
        """Computes Energy function gradient at w"""  
        return sum(numpy.dot((self.logit(numpy.dot(self.X,w)) - self.Y), self.X))
        #check the dimension of this array/how the sum is done

    def init_config(self):
        """Inits position of sampler """
        x = numpy.zeros(self.N_DIM)
        if self.INIT_EQ:
            x = numpy.random.randn(self.N_DIM) * self.VARIANCE_PRIOR ** 0.5
        return x
            
    def initEC(self, scheme_arg=None):
        """Inits sampler """
        if scheme_arg is None:
            lift_vector = numpy.random.normal(0.0, 1.0, self.N_DIM)
            lift_vector /= numpy.dot(lift_vector,lift_vector) ** 0.5
        elif scheme_arg == 'BOUNCE' or scheme_arg == 'FORWARD':
            lift_vector = numpy.random.normal(0.0, 1.0,  self.N_DIM)
            lift_vector /= numpy.dot(lift_vector,lift_vector) ** 0.5
        elif scheme_arg == 'STRAIGHT':
            lift_vector = numpy.zeros(self.N_DIM)
            lift_vector[int(random.uniform(0.0, 1.0) * self.N_DIM)] = random.choice([-1.0, 1.0])
        return lift_vector
            
    def do_lift(self, old_w, distance, update_vector):
        """ Update position along update_vector """
        return old_w + distance * update_vector 
        
    def deltaE_Metro(self, old_w, new_w):
        """ Computes difference in energy or minus log of probability """
        return self.Eposterior(new_w) - self.Eposterior(old_w)
        
    def Eprior(self, w):
        """Computes prior energy function at w"""
        return numpy.dot(self.VARIANCEINV_LIST_PRIOR, w ** 2.0)

    def Eposterior(self, w):
        """Computes posterior Energy function at w"""
        return self.E(w) + self.Eprior(w)

    def posterior(self, w):
        """Computes posterior at w"""
        return numpy.exp(-self.Eposterior(w))

    def propose_move(self, w, delta):
        """ Propose a discrete update of the position """
        update_vector = numpy.random.normal(0.0, 1.0, self.N_DIM)
        update_vector /= numpy.dot(update_vector, update_vector) ** 0.5
        return w.copy() + random.uniform(0.0,1.0) * delta * update_vector
               
    def get_prior_event(self, w, update_vector):
        """ Computes event for PDMC for the prior component"""
        DeltaE = -math.log(random.uniform(0.0,1.0))
        a =  0.5 * numpy.dot(self.VARIANCEINV_LIST_PRIOR, update_vector ** 2.0)
        initial_min_dist = numpy.dot(update_vector, self.VARIANCEINV_LIST_PRIOR*w)/ (2.0 * a)
        E0 = numpy.dot(self.VARIANCEINV_LIST_PRIOR, w ** 2.0)
        Emin = E0 - a * initial_min_dist ** 2.0
        if initial_min_dist > 0.0:
            DeltaE += E0
        else:
            DeltaE += Emin
        return - initial_min_dist + ((DeltaE - Emin) / a) ** 0.5 


    def get_next_event(self, w, update_vector):
        """ Computes event for PDMC"""
        distance_to_next_event = self.get_prior_event(w, update_vector)
        self.event_factor = -1
        for i in range(self.N_DATA):
            x_dot_w = numpy.dot(self.X[i], w)
            x_dot_vector = numpy.dot(self.X[i], update_vector)
            if x_dot_vector * (0.5 - self.Y[i]) < 0.0: continue
            delta_E =-math.log(random.uniform(0.0,1.0))
            if delta_E >= abs(x_dot_vector) : continue
	    if self.Y[i]==0:
                dist=distance_to_next_event*x_dot_vector + x_dot_w
                if dist < x_dot_w+delta_E:continue
		expXW=numpy.exp(x_dot_w+delta_E)
		expE=numpy.exp(delta_E)
                distance_factor_i = (numpy.log(-1 + expE + expXW) - x_dot_w)/x_dot_vector
	    else:
                dist=-distance_to_next_event*x_dot_vector - x_dot_w
                if dist < -x_dot_w + delta_E:continue
		expE=numpy.exp(delta_E)
                expXW_INV=numpy.exp(-x_dot_w+delta_E)		
                distance_factor_i = -(numpy.log(-1 + expE + expXW_INV) + x_dot_w)/x_dot_vector
            if distance_factor_i < 0.0:
                    print delta_E, delta_exp, x_dot_vector, x_dot_w, self.Y[i]                    
                    sys.exit()
            if distance_factor_i < distance_to_next_event:
                    distance_to_next_event, self.event_factor = distance_factor_i, i 
        return distance_to_next_event
