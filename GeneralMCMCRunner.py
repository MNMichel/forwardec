"""    
    Copyright (C) 2017-2019 Manon Michel <manon.michel@uca.fr>

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

"""




import sys, importlib, os, math, numpy, random, time, pickle, argparse

## General functions

def str_to_function(cls, str):
    """Converts a string to a function """
    return getattr(cls, str)

## Classes
class ParserMCGeneral:   
    
    def __init__(self): 
        """ inits Parser with the arguments to take into account"""
        self.parser = argparse.ArgumentParser(description='General Monte Carlo sampling of a target probability distribution. Needs parameters specification for target distribution and for sampling process')                                                  
        self.parser.add_argument('-Library', default = 'Gaussian', type = str, dest = 'Library',
                                 help='Specify Distribution file. Default = Gaussian')
        self.parser.add_argument('-DistributionClass', default = 'GaussianLogLinearVar', type = str, dest = 'DistributionClass',
                                help='Class of the target distribution in the distribution file. '
                                 +'For specific help on distribution parameters, type '
                                 +'python GeneralMCMCRunner.py -DistributionClass [YOUR CHOICE] -h. Default = GaussianLogLinearVar')     
        self.parser.add_argument('-OUTPUT_DISTANCE', default = 100, type = float,  dest = 'OUTPUT_DISTANCE',
                                help='Number of attempted moves or time T between two samples. Default = 100') 
        self.parser.add_argument('-TOTAL_NUMBER_SAMPLE', default = 1000, type = int,  dest = 'TOTAL_NUMBER_SAMPLE',
                                help='Desired number of samples once the run is completed. Default = 1000')
        self.parser.add_argument('-PERCENT_SAVE', default = 10, type = int,  dest = 'PERCENT_SAVE',
                               help='Iterative percentage of the run at which the collected samples are stored. Default = 10')            
        self.parser.add_argument('-SCHEME', default = 'Metropolis', type = str, dest = 'SCHEME',
                                help='Update scheme of MCMC. Default = Metropolis') 
        self.parser.add_argument('-OBSERVABLES', default='x', type = str, dest = 'OBSERVABLES', help=' Observables to compute and stores. Default = full configuration (x)')
        self.parser.add_argument('-DELTA', default=10.0, type = float, dest = 'DELTA', help="Specifies range for Metropolis update. Default=10.0")      
        self.parser.add_argument('-PROBA_SWITCH', default=0.018, type = float, dest = 'PROBA_SWITCH', help="Set probability of orthogonal kernel in mixture with identity. Default=0.018")   
        self.parser.add_argument('-REFRESH', default=500.0, type = float, dest = 'REFRESH', help="Set time T for refreshment. Default = 500.0")
        self.parser.add_argument('-DIM_REFRESH', default=2, type = int, dest = 'DIM_REFRESH', help="Set p in ran-p-orthogonal schemes. Default=2")   
        self.parser.add_argument('-THETA', default=0.1, type = float, dest = 'THETA',help="Set theta in  theta-orthogonal schemes. Default=0.1")
        self.parser.add_argument('-CONTINUE', action='store_true', dest = 'CONTINUE',help="If specified, resume a previous run.")            
        self.parser.add_argument('-NPY', action='store_true', dest = 'NPY', help="If specified, store data in npy format (compressed)")     
        if '-Library' in sys.argv:
            self.Library = self.str_to_module(sys.argv[sys.argv.index('-Library')+1])
        elif '-h' in sys.argv:
            self.parser.parse_args()
        else:
            self.Library = self.str_to_module('Gaussian')
        self.Library.Parser(self.parser)
        self.parameters_sampling = vars(self.parser.parse_args())
        self.parameters_sampling['Librabry'] = self.Library
        if '-DistributionClass' in sys.argv:
            self.parameters_sampling['Distribution'] = self.str_to_class(sys.argv[sys.argv.index('-DistributionClass')+1])(self.parameters_sampling)
        else:
            self.parameters_sampling['Distribution'] = self.Library.GaussianLogLinearVar(self.parameters_sampling)

    def str_to_class(self, str):
        """Converts a string to a class """
        return getattr(self.Library, str)
        
    def str_to_module(self, str):
        """Converts a string to an imported module """
        return importlib.import_module('DistributionClassLibrary.'+str)

class RunMCGeneral:
    
    def __init__(self, parameters_sampling):
        self.TargetDistribution = parameters_sampling['Distribution']
        self.OBSERVABLES = {}
        for obs in parameters_sampling['OBSERVABLES'].split("_"):
            if obs=='None':
                self.OBSERVABLES = {}
                break
            elif obs == 'x':
                self.OBSERVABLES['x'] = getattr(self, 'sample') 
            elif 'vec' in obs:
                self.OBSERVABLES[obs] = 'DefinedInAlgoClass'             
            else:
                self.OBSERVABLES[obs] = str_to_function(self.TargetDistribution, obs)
        self.SCHEME = parameters_sampling['SCHEME']
        if 'Metropolis' in self.SCHEME or 'HMC' in self.SCHEME:
            self.OUTPUT_DISTANCE = int(parameters_sampling['OUTPUT_DISTANCE'])
        else:
            self.OUTPUT_DISTANCE = parameters_sampling['OUTPUT_DISTANCE']
        if 'Metropolis' in self.SCHEME:
            self.DELTA = parameters_sampling['DELTA']            
        if 'RanP' in self.SCHEME:
            self.DIM_REFRESH = parameters_sampling['DIM_REFRESH']        
        if 'FixTheta' in self.SCHEME:
            self.THETA_litt = parameters_sampling['THETA']
            self.THETA = math.pi * self.THETA_litt
        if 'Proba' in self.SCHEME:
            self.p_switch = parameters_sampling['PROBA_SWITCH']
        if 'Refresh' in self.SCHEME:
            self.distance_refresh = parameters_sampling['REFRESH']
        self.TOTAL_NUMBER_SAMPLE = parameters_sampling['TOTAL_NUMBER_SAMPLE']
        self.PERCENT_SAVE = parameters_sampling['PERCENT_SAVE']   
        self.CONTINUE = parameters_sampling['CONTINUE']  
        self.NPY = parameters_sampling['NPY']  
        self.parameters_store = self.file_storage_init(parameters_sampling)
        self.C, self.data, l_data = self.data_init(parameters_sampling)
        self.number_events = 0
        self.number_sample = l_data
        self.number_sample_stored = l_data
        self.number_saved_sample= int(self.TOTAL_NUMBER_SAMPLE * self.PERCENT_SAVE  / 100) 

    def sample(self, C):
        return C.copy()

    def file_storage_init(self, parameters_sampling):
        """
        Initializes the filenames for storage
        Returns: filenames for storage
        """  
        parameters_store = parameters_sampling['Librabry'].get_rootfile(parameters_sampling, self.TargetDistribution.name_class)      
        parameters_store['RootNameForFile'] = parameters_store['RootNameForFile'] +self.SCHEME 
        if 'Event' in self.SCHEME:
            if hasattr(self.TargetDistribution, "FACTOR"):
                if self.TargetDistribution.FACTOR:
                    parameters_store['RootNameForFile']+='_Factor'
        if 'Metropolis' in self.SCHEME:
            parameters_store['RootNameForFile']+='_Delta%f'%self.DELTA
        if '_Refresh' in self.SCHEME and not 'RefreshAtAll' in self.SCHEME:
            parameters_store['RootNameForFile']+='_Refresh%f'%self.distance_refresh
        if 'RanP' in self.SCHEME:
                parameters_store['RootNameForFile']+='_DimSwitch%i'%self.DIM_REFRESH
        if 'FixTheta' in self.SCHEME:
                parameters_store['RootNameForFile']+='_Theta%0.2fpi'%self.THETA_litt
        parameters_store['RootNameForFile']+= '_OutputDistance%.2f'%self.OUTPUT_DISTANCE 
        if hasattr(self.TargetDistribution, "INIT_EQ"):     
            if self.TargetDistribution.INIT_EQ:
                parameters_store['RootNameForFile']+='_INITEQ'
        parameters_store['LogFile'] = parameters_store['RootNameForFile' ] + '.log' 
        parameters_store['LastChainStateFile'] = (parameters_store['RootNameForFile' ] 
                                                   + '_LastChainState' + '.data')
        for obs in self.OBSERVABLES.keys():
            parameters_store[obs+'File'] = (parameters_store['RootNameForFile' ] 
                                                        +'_'+obs+'.data')
        return parameters_store
    
    def data_init(self, parameters_sampling):
        """Initializes the state x, the update vector and the running output 
           distance
            Returns: Initialized the dictionary chain_state and the list data
        """  
        print '\n'+'Run parameters:'
        print 'Distribution parameters: '+self.TargetDistribution.name_class
        for key in parameters_sampling.keys():
            print '    '+key + ' : '+ str(parameters_sampling[key])
        print '    Root name for file storage: '+self.parameters_store['RootNameForFile']
        print 'Final number of samples:' +str(self.TOTAL_NUMBER_SAMPLE)+'\n'

        if os.path.isfile(self.parameters_store['LastChainStateFile']):
            print 'Previous data with same parameters -- Continuing run\n'
            if self.CONTINUE:
                f = open(self.parameters_store['LogFile'],'r')
                lines = f.readlines()
                f.close()
                NIter = sum(numpy.array([float(line.split()[0]) for line in lines if not line.startswith('#')]))   
                self.TOTAL_NUMBER_SAMPLE -= NIter - 1
                if self.TOTAL_NUMBER_SAMPLE <= 0:
                    print 'Already done'; sys.exit()
                else:
                    print 'Left to do ', self.TOTAL_NUMBER_SAMPLE
            else:
                print 'Caution, file already there'; sys.exit()                    
            C = pickle.load(open(self.parameters_store['LastChainStateFile'], "rb" ) )
            with open(self.parameters_store['LogFile'], 'a') as f:
                f.write('# Continuing run' + '\n')        
                f.write('# Loaded ' + self.parameters_store['LastChainStateFile']+'\n')
                f.close()
            data ={}
            for obs in self.OBSERVABLES.keys():
                data[obs] = []
            l_data = 0      
        else:
            C = self.TargetDistribution.init_config()
            data = {}
            for obs in self.OBSERVABLES.keys():
                if 'vec' in obs:
                    data[obs]=[]
                else:
                    data[obs] = [self.OBSERVABLES[obs](C)]
            l_data = 1        
            with open(self.parameters_store['LogFile'], 'a') as f:
                f.write('# New run Parameters \n')
                f.write('#' +self.parameters_store['RootNameForFile']+'\n')
                if 'Metropolis' in self.SCHEME or 'HMC' in self.SCHEME:
                    f.write('# #Samples stored -- Time -- #Events since last storage (proposed moved) -- #Accept rate since beginning\n')
                elif 'Event' in self.SCHEME:
                    f.write('# #Samples stored -- Time -- #Events since last storage (lift)')
                    if hasattr(self.TargetDistribution, "extra_arg"):
                        if hasattr(self.TargetDistribution, "FakeEv"):
                            f.write(' -- Fake Events since last storage')
                        if hasattr(self.TargetDistribution, "d"):
                            f.write(' -- Av dist between events')
                    f.write('\n')
                f.close()
        return C, data, l_data

    def store(self, acc=None): 
        with open(self.parameters_store['LogFile'], 'a') as flog:
            flog.write(str(self.number_sample_stored)+' '+str(time.clock()-self.t)+' '+ str(float(self.number_events )))
            if 'Event' in self.SCHEME:
                if hasattr(self.TargetDistribution, "FakeEv"):
                    flog.write(' '+str(float(self.TargetDistribution.FakeEv)))
                    self.TargetDistribution.FakeEv = 0
                if hasattr(self.TargetDistribution, "d"):
                    flog.write(' '+str(float(self.TargetDistribution.d)/float(self.number_events)))
                    self.TargetDistribution.d = 0
            if acc:
                flog.write(' '+ str(float(acc))+'\n')
            else:
                flog.write('\n')  
            self.t = time.clock()           
        for obs in self.OBSERVABLES.keys():
            with open(self.parameters_store[obs+'File'], 'a') as f:
                numpy.savetxt(f, self.data[obs])   
        pickle.dump(self.C, open(self.parameters_store['LastChainStateFile'],"wb" ))              
        return          

class MCMCMethodLibrary:
    """
    Class for Metropolis algorithm
    """ 
     
    def __init__(self, Run):
        """
        Initialization  of the class
        """ 
        self.Run = Run
        for key in self.Run.OBSERVABLES.keys():
            if 'vec' in key:
                del self.Run.OBSERVABLE[key]
        self.TargetDistribution = self.Run.TargetDistribution
        self.acc = 0.0       
        self.SCHEME = str_to_function(self, self.Run.SCHEME) 

    def new_sample(self):
        """
        Generate one sample
        """ 
        for step in range(self.Run.OUTPUT_DISTANCE):
            self.Run.number_events  += 1
            new_C = self.TargetDistribution.propose_move(self.Run.C, self.Run.DELTA)
            pacc = self.SCHEME(new_C)
            if pacc:
                self.Run.C = new_C
                self.acc += 1.0
        return        
        
    def N_new_sample(self):
        """
        Generate the required N samples, generate log and data
        """
        t = time.clock()
        self.Run.t = time.clock()        
        print 'Run started'
        while self.Run.number_sample < self.Run.TOTAL_NUMBER_SAMPLE: 
            self.new_sample()
            for obs in self.Run.OBSERVABLES.keys():
                self.Run.data[obs] += [self.Run.OBSERVABLES[obs](self.Run.C)]
            self.Run.number_sample += 1
            if not self.Run.number_sample % self.Run.number_saved_sample: 
                print (str(self.Run.number_sample // self.Run.number_saved_sample 
                                               * self.Run.PERCENT_SAVE)
                        +'% Complete, time: '+ str(time.clock() - t))
                self.Run.store(self.acc/float(self.Run.number_sample * self.Run.OUTPUT_DISTANCE))
                for obs in self.Run.OBSERVABLES.keys():
                    self.Run.data[obs] = []
                self.Run.number_events  = 0
        if self.Run.number_sample % self.Run.number_saved_sample:
                self.Run.store(self.acc / float(self.Run.TOTAL_NUMBER_SAMPLE * self.Run.OUTPUT_DISTANCE)) 
                print '100% Complete, time ' +str(time.clock() - t)
        print 'Run completed -- acceptation rate '+str(self.acc / float(self.Run.TOTAL_NUMBER_SAMPLE * self.Run.OUTPUT_DISTANCE))
        if self.Run.NPY:
            for obs in self.Run.OBSERVABLES.keys():
                datafile = self.Run.parameters_store[obs+'File']
                data_to_convert = numpy.loadtxt(datafile)
                if os.path.isfile(datafile+'.npy'):
                    print 'Getting past data'
                    data_archive = numpy.load(datafile+'.npy')
                    data_to_convert = list(data_archive) + list(data_to_convert)
                numpy.save(datafile+'.npy',data_to_convert)            
                os.remove(datafile)
                print "npy converted", datafile
        return    

    def Metropolis(self, new_C):
        """
        Compute acceptance of move using difference of minus log-probability weights
        """ 
        pacc = math.exp(- self.TargetDistribution.deltaE_Metro(self.Run.C, new_C))
        return random.uniform(0.0,1.0) <= pacc

    def Metropolis_ratio(self, new_C):
        """
        Compute acceptance of move using ratio of probability weights
        """ 
        pacc = min(1, self.TargetDistribution.ratio_Metro(self.Run.C, new_C))
        return random.uniform(0.0,1.0) <= pacc


class HMCMethodLibrary:     
    """
    Class for HMC algorithm
    """ 
    def __init__(self, Run):
        """
        Initialization  of the class
        """ 
        self.Run = Run
        for key in self.Run.OBSERVABLES.keys():
            if 'vec' in key:
                del self.Run.OBSERVABLE[key]
        self.TargetDistribution = self.Run.TargetDistribution
        self.acc = 0.0   
        self.step_done = 0.0
        self.T = 10
        self.h_min = 0.00001
        self.acc_target = 0.63
        self.alpha = 1.0 
        self.h = 1.0/(self.T * (1.0 + sum(self.TargetDistribution.dE_vec(self.Run.C) ** 2.0 )))
        self.Run.OBSERVABLES['acc'] = getattr(self, 'get_acc') 
        self.Run.OBSERVABLES['h'] = getattr(self, 'get_h') 
        for obs in ['acc','h']:
            self.Run.parameters_store[obs+'File'] = (self.Run.parameters_store['RootNameForFile' ] 
                                                        +'_'+obs+'.data')
        self.Run.data['acc'] = [0.0]
        self.Run.data['h'] = [self.h]

    def get_acc(self, *kwarg):
        return self.acc

    def get_h(self, *kwarg):
        return self.h

    def new_sample(self):
        """
        Generate one sample
        """ 
        for step in range(self.Run.OUTPUT_DISTANCE):
            self.step_done += 1.0
            self.acc *= (self.step_done - 1.0) / self.step_done
            self.Run.number_events  += self.T
            self.P = numpy.random.randn(self.TargetDistribution.N_DIM)
            new_C, new_P = self.leapfrog()
            pacc =  math.exp(-self.TargetDistribution.deltaE_Metro(self.Run.C, new_C) - self.deltaK(self.P, new_P))
            if random.uniform(0.0, 1.0) < pacc:
                self.Run.C = new_C
                self.acc += 1.0/ self.step_done 
            delta_acc = (self.acc_target-self.acc)
            if delta_acc != 0.0:
                delta_acc /= abs(delta_acc)
            self.h = max(self.h_min,(1.0 - 0.0001 / self.step_done**self.alpha * delta_acc) * self.h - 1.0 / self.step_done**self.alpha * delta_acc)
        return   
        
    def leapfrog(self):
        """
        Leapfrog integrator
        """ 
        new_C = self.Run.C.copy()
        new_P = self.P.copy()
        dE_store = self.TargetDistribution.dE_vec(new_C)
        for t in range(self.T):
            new_P -= self.h/2.0 * dE_store 
            new_C += self.h * new_P
            dE_store = self.TargetDistribution.dE_vec(new_C)
            new_P -= self.h/2.0 * dE_store 
        return new_C, new_P

    def deltaK(self, p_old, p_new) :
        return (sum(p_new ** 2.0) - sum(p_old ** 2.0)) * 0.5

    def N_new_sample(self):     
        """
        Generate the required N samples, generate log and data
        """ 
        t = time.clock()
        self.Run.t = time.clock()        
        print 'Run started'
        while self.Run.number_sample < self.Run.TOTAL_NUMBER_SAMPLE: 
            self.new_sample()
            for obs in self.Run.OBSERVABLES.keys():
                self.Run.data[obs] += [self.Run.OBSERVABLES[obs](self.Run.C)]
            self.Run.number_sample += 1
            self.Run.number_sample_stored += 1
            if not self.Run.number_sample % self.Run.number_saved_sample: 
                print (str(self.Run.number_sample // self.Run.number_saved_sample 
                                               * self.Run.PERCENT_SAVE)
                        +'% Complete, time: '+ str(time.clock() - t)+'  acc rate  '+str(self.acc))
                self.Run.store(self.acc)
                for obs in self.Run.OBSERVABLES.keys():
                    self.Run.data[obs] = []
                self.Run.number_events  = 0
                self.Run.number_sample_stored = 0
        if self.Run.number_sample_stored != 0:
                self.Run.store(self.acc) 
                print '100% Complete, time ' +str(time.clock() - t)
        print 'Run completed -- acceptation rate '+str(self.acc)
        if self.Run.NPY:
            for obs in self.Run.OBSERVABLES.keys():
                datafile = self.Run.parameters_store[obs+'File']
                data_to_convert = numpy.loadtxt(datafile)
                if os.path.isfile(datafile+'.npy'):
                    print 'Getting past data'
                    data_archive = numpy.load(datafile+'.npy')
                    data_to_convert = list(data_archive) + list(data_to_convert)
                numpy.save(datafile+'.npy',data_to_convert)            
                os.remove(datafile)
                print "npy converted", datafile
        return  


  
class OptimizedIrrMethodLibrary:
    """
    Class for Optimized EC algorithm
    """
     
    def __init__(self, Run):
        """
        Initialization  of the class
        """ 
        self.Run = Run
        for key in self.Run.OBSERVABLES.keys():
            if 'vec' in key:
                del self.Run.OBSERVABLE[key]
        self.TargetDistribution = self.Run.TargetDistribution
        self.update_dim = 0
        self.C_Store=[]
        self.OptimizedIrr_nextevent=self.OptimizedIrrFull
        self.update_dir = [random.choice([-1.0,1.0]) for dim in range(self.TargetDistribution.N_DIM)]
        self.output_distance_array, self.update_vector_array = self.TargetDistribution.init_StraightEC()
        self.iter_chain = int(self.Run.OUTPUT_DISTANCE)
        self.running_output_distance = self.output_distance_array[self.update_dim]


    def new_sample(self):        
        """
        Generate one sample
        """ 
        iter_chain = self.iter_chain 
        while True:           
            chain_over = self.OptimizedIrr_nextevent()
            if chain_over : 
                iter_chain -= 1             
                self.update_dim = (self.update_dim + 1) % self.TargetDistribution.N_DIM
                self.running_output_distance = self.output_distance_array[self.update_dim]
                if not iter_chain > 0 :
                    break
        return   

    def N_new_sample(self):   
        """
        Generate the required N samples, generate log and data
        """ 
        t = time.clock()                 
        self.Run.t = time.clock()           
        print 'Run started'
        p=1
        while self.Run.number_sample < self.Run.TOTAL_NUMBER_SAMPLE: 
            self.new_sample()
            for obs in self.Run.OBSERVABLES.keys():
                    self.Run.data[obs] += [self.Run.OBSERVABLES[obs](self.C_Store)]
            self.Run.number_sample += 1
            self.Run.number_sample_stored += 1
            if self.Run.number_sample == self.Run.TOTAL_NUMBER_SAMPLE: break
            if  self.Run.number_sample > p*self.Run.number_saved_sample: 
                print (str(self.Run.number_sample // self.Run.number_saved_sample 
                                               * self.Run.PERCENT_SAVE)
                        +'% Complete, time: '+ str(time.clock() - t))
                if 'rate' in self.__dict__:
                    print 'Acceptance rate', self.rate/float(self.Run.number_events)
                    self.rate=0
                self.Run.store()
                p += 1
                for obs in self.Run.OBSERVABLES.keys():
                    self.Run.data[obs] = [] 
                self.Run.number_events  = 0
                self.Run.number_sample_stored = 0
        if self.Run.number_sample_stored !=0:
                self.Run.store() 
                print '100% Complete, time ' +str(time.clock() - t)
                if 'rate' in self.__dict__:
                    print 'Acceptance rate', self.rate/float(self.Run.number_events)
                    self.rate=0
        if self.Run.NPY:
            for obs in self.Run.OBSERVABLES.keys():
                datafile = self.Run.parameters_store[obs+'File']
                data_to_convert = numpy.loadtxt(datafile)
                if os.path.isfile(datafile+'.npy'):
                    print 'Getting past data'
                    data_archive = numpy.load(datafile+'.npy')
                    data_to_convert = list(data_archive) + list(data_to_convert)
                numpy.save(datafile+'.npy',data_to_convert)            
                os.remove(datafile)
                print "npy converted", datafile
        print 'Run completed'
        return  

    def OptimizedIrrFull(self):
        """ Run a PDMP until refreshment
        """
        chain_over = False
        while not chain_over:
            self.Run.number_events += 1
            new_distance = self.TargetDistribution.get_next_event_unidimensional(self.Run.C, self.update_dim, self.update_dir[self.update_dim])
            if new_distance > self.running_output_distance:
                self.Run.C = self.TargetDistribution.do_lift(self.Run.C, self.running_output_distance, self.update_vector_array[self.update_dim] * self.update_dir[self.update_dim])
                self.C_Store = self.Run.C.copy()
                chain_over = True
            else:    
                self.Run.C = self.TargetDistribution.do_lift(self.Run.C, new_distance, self.update_vector_array[self.update_dim] * self.update_dir[self.update_dim])
                self.update_dir[self.update_dim] = -self.update_dir[self.update_dim]
                self.running_output_distance -= new_distance                
        return chain_over  

class ECMCMethodLibrary:
    """
    Class for PDMC algorithm
    """
    
    def __init__(self, Run):
        """
        Initialization  of the class
        """ 
        self.Run = Run
    #-------------------------------------------------------------------------------#
        if 'vec' in self.Run.OBSERVABLES.keys():
            self.Run.OBSERVABLES['vec']= getattr(self, 'get_vec') 
        self.TargetDistribution = self.Run.TargetDistribution
        scheme_main =  self.Run.SCHEME.split('__')[0]
        scheme_arg = self.Run.SCHEME.split('__')[1:]
        """ Setting initialization depending on specified scheme """
        if 'Straight' in scheme_arg[0]:
            if not 'Ill'  in scheme_arg[0]:
                self.update_vector = self.TargetDistribution.initEC('STRAIGHT')
            else:
                print 'init to ILL_STRAIGHT'
                self.update_vector = self.TargetDistribution.initEC('ILL_STRAIGHT')
        else:    
            self.update_vector = self.TargetDistribution.initEC()
    #-------------------------------------------------------------------------------#
        self.SCHEME = str_to_function(self, scheme_main)   
        """ Setting refreshment distance """ 
        if 'Event_Refresh' in scheme_main:
            if 'Proba' in scheme_arg:
                self.p_switch = self.Run.p_switch
            elif 'All' not in scheme_main:                
                self.running_distance_refresh = self.Run.distance_refresh
    #-------------------------------------------------------------------------------#
            """ Setting refreshment scheme """
            if 'FullRotate' in scheme_arg:
                self.refresh_vector = self.full_ref
            elif 'PosFullRotate' in scheme_arg:
                self.refresh_vector = self.pos_full_ref
            else:
                """ Setting Orthogonal refresh or K_\perp """
                if self.TargetDistribution.N_DIM > 2:
                    self.refresh_vector = self.ortho_ref
                    if 'PosFullOrtho' in scheme_arg:
                        self.get_vector_ortho = self.posfull_vector_ortho
                    if 'NaiveFullOrtho' in scheme_arg:
                        self.get_vector_ortho = self.naivefull_vector_ortho
                    if 'PosSwitchOrtho' in scheme_arg:
                            self.get_vector_ortho = self.posswitch_vector_ortho
                    if 'NaiveSwitchOrtho' in scheme_arg:
                            self.get_vector_ortho = self.naiveswitch_vector_ortho
                    if 'PosPerpSwitchOrtho' in scheme_arg:
                        self.get_vector_ortho = self.posperpswitch_vector_ortho
                    if 'RanPOrtho' in scheme_arg:
                        self.get_vector_ortho = self.ranp_vector_ortho
                        self.dim_refresh=self.Run.DIM_REFRESH
                    if 'FixThetaOrtho' in scheme_arg:
                        self.get_vector_ortho = self.theta2_vector_ortho
                        self.THETA=self.Run.THETA
                else:    
                    self.refresh_vector = self.pass_function
    #-------------------------------------------------------------------------------#
        """ Setting K_\para """
        if 'ReflectionPick' in  scheme_arg[0]:
            self.new_event_vector_pick = self.reflection_pick
        elif 'DirectPick' in scheme_arg[0]:
            self.new_event_vector_pick = self.direct_pick
        elif 'IsoMetroPick' in scheme_arg[0]:
            self.new_event_vector_pick = self.isometro_pick
            self.rate=0
        elif 'StepMetroPick' in scheme_arg[0]:
            self.new_event_vector_pick = self.stepmetro_pick
            self.rate=0
        elif 'StraightPick' in scheme_arg[0]:
            self.new_event_vector_pick = self.straight_pick            
    #-------------------------------------------------------------------------------#
        """ Setting Factorization """
        if hasattr(self.TargetDistribution, "FACTOR"):
            if self.TargetDistribution.FACTOR:
                if 'Straight' in scheme_arg[0]:
                    self.get_gradE_norm = self.TargetDistribution.get_factor
                else:
                    self.get_gradE_norm = self.get_gradE_norm_factor          
            else:
                if 'Straight' in scheme_arg[0]:
                    self.get_gradE_norm = self.none_function
                else: 
                    self.get_gradE_norm = self.get_gradE_norm_gen                
        else:
            if 'Straight' in scheme_arg[0]:
                self.get_gradE_norm = self.none_function
            else: 
                self.get_gradE_norm = self.get_gradE_norm_gen   
        self.running_output_distance = self.Run.OUTPUT_DISTANCE 
        self.C_Store=[]

    def get_gradE_norm_gen(self): 
        """ Return normalized gradient at position self.Run.C """
        grad_E = self.TargetDistribution.dE_vec(self.Run.C)
        grad_E_norm =  grad_E / sum(grad_E ** 2.0) ** 0.5
        return grad_E_norm

    def get_gradE_norm_factor(self): 
        """ Return normalized factorized gradient at position self.Run.C """
        grad_E =  self.TargetDistribution.dE_vec_factor(self.Run.C)
        grad_E_norm =  grad_E / sum(grad_E ** 2.0) ** 0.5
        return grad_E_norm

    def get_vec(self,arg):
        return self.update_vector

    def get_vec_event(self,arg):
        return self.update_vector_store

    def new_sample(self):
        """
        Generate one sample
        """ 
        while True:
            self.Run.number_events += 1
            chain_over = self.SCHEME()
            if chain_over : 
                break
        return   
        
    def N_new_sample(self):   
        """
        Generate the required N samples, generate log and data
        """ 
        t = time.clock() 
        self.Run.t = time.clock()           
        print 'Run started'
        p=1
        while self.Run.number_sample < self.Run.TOTAL_NUMBER_SAMPLE: 
            self.new_sample()
            for C_sample in self.C_Store: 
                for obs in self.Run.OBSERVABLES.keys():
                    self.Run.data[obs] += [self.Run.OBSERVABLES[obs](C_sample)]
                self.Run.number_sample += 1
                self.Run.number_sample_stored += 1
                if self.Run.number_sample == self.Run.TOTAL_NUMBER_SAMPLE: break
            self.C_Store = []
            if  self.Run.number_sample > p*self.Run.number_saved_sample: 
                print (str(self.Run.number_sample // self.Run.number_saved_sample 
                                               * self.Run.PERCENT_SAVE)
                        +'% Complete, time: '+ str(time.clock() - t))
                if 'rate' in self.__dict__:
                    print 'Acceptance rate', self.rate/float(self.Run.number_events)
                    self.rate=0
                self.Run.store()
                p += 1
                for obs in self.Run.OBSERVABLES.keys():
                    self.Run.data[obs] = [] 
                self.Run.number_events  = 0
                self.Run.number_sample_stored = 0
        if self.Run.number_sample_stored !=0:
                self.Run.store() 
                print '100% Complete, time ' +str(time.clock() - t)
                if 'rate' in self.__dict__:
                    print 'Acceptance rate', self.rate/float(self.Run.number_events)
                    self.rate=0
        if self.Run.NPY:
            for obs in self.Run.OBSERVABLES.keys():
                datafile = self.Run.parameters_store[obs+'File']
                data_to_convert = numpy.loadtxt(datafile)
                if os.path.isfile(datafile+'.npy'):
                    print 'Getting past data'
                    data_archive = numpy.load(datafile+'.npy')
                    data_to_convert = list(data_archive) + list(data_to_convert)
                numpy.save(datafile+'.npy',data_to_convert)            
                os.remove(datafile)
                print "npy converted", datafile
        print 'Run completed'
        return   

    def pass_function(self,**kwarg):
        pass

    def none_function(self,**kwarg):
        return None

    def full_ref(self,**kwarg):
        """ Pick a random vector on the hypersphere """
        self.update_vector = numpy.random.normal(0.0, 1.0, self.TargetDistribution.N_DIM)
        self.update_vector /= sum(self.update_vector ** 2.0) ** 0.5

    def pos_full_ref(self,**kwarg):
        """ Pick a random vector on the hypersphere and align it to the former direction vector"""
        self.old_vector = self.update_vector.copy()
        self.update_vector = numpy.random.normal(0.0, 1.0, self.TargetDistribution.N_DIM)
        self.update_vector *= numpy.dot(self.update_vector, self.old_vector)
        self.update_vector /= sum(self.update_vector ** 2.0) ** 0.5

    def ortho_ref(self, vec_ref=None):
        """ Apply K_perp: orthogonal refreshment """
        if vec_ref is None:
            grad_E = self.get_gradE(self.Run.C) 
            vec_ref = grad_E / sum(grad_E ** 2.0) ** 0.5
        b =  numpy.dot(self.update_vector, vec_ref)
        a =  (1.0 - b ** 2.0) ** 0.5
        vector_ortho = self.get_vector_ortho(vec_ref)
        self.update_vector = a * vector_ortho +  b * vec_ref

    def get_GramSchmidt_vector(self, vec_ref):
        """ Return two orthogonal vectors following GramSchmidt procedure"""
        G_vec = numpy.random.normal(0,1,size=(2,self.TargetDistribution.N_DIM))
        G_vec_norm = (G_vec.T/sum((G_vec**2).T)**0.5).T
        G_vec_norm -= numpy.outer(numpy.dot(G_vec_norm, vec_ref), vec_ref)
        G_vec_norm[0] /= sum(G_vec_norm[0]**2.0)**0.5
        G_vec_norm[1] -= numpy.dot(G_vec_norm[0], G_vec_norm[1])* G_vec_norm[0]
        G_vec_norm[1] /= sum(G_vec_norm[1]**2.0)**0.5
        return G_vec_norm        

    def get_GramSchmidt_vector_highdim(self, vec_ref):
        """ Return self.dim_refresh orthogonal vectors following GramSchmidt procedure"""
        G_vec = numpy.random.normal(0,1,size=(self.dim_refresh,self.TargetDistribution.N_DIM))
        G_vec_norm = (G_vec.T/sum((G_vec**2).T)**0.5).T
        G_vec_norm -= numpy.outer(numpy.dot(G_vec_norm, vec_ref), vec_ref)
        G_vec_norm[0] /= sum(G_vec_norm[0]**2.0)**0.5
        for i in range(1,self.dim_refresh):
            for j in range(i):
                G_vec_norm[i] -= numpy.dot(G_vec_norm[j], G_vec_norm[i])* G_vec_norm[j]
            G_vec_norm[i] /= sum(G_vec_norm[i]**2.0)**0.5
        return G_vec_norm

    def naiveswitch_vector_ortho(self, vec_ref):
        """ Switch the components along the Gramschmidt-obtained vectors"""
        vector_ortho = self.update_vector.copy()
        vector_ortho -= numpy.dot(vector_ortho, vec_ref) * vec_ref 
        G_vec_norm = self.get_GramSchmidt_vector(vec_ref)
        G_vector_scalar = numpy.dot(G_vec_norm, vector_ortho)
        vector_ortho += numpy.dot(G_vector_scalar, G_vec_norm[::-1] -  G_vec_norm)
        vector_ortho /= sum(vector_ortho ** 2.0) ** 0.5  
        return vector_ortho

    def posfull_vector_ortho(self, vec_ref):
        """ Pick a random vector in the hyperplan. Align to former direction"""
        vector_ortho = numpy.random.normal(0.0, 1.0, self.TargetDistribution.N_DIM)
        vector_ortho -= numpy.dot(vector_ortho, vec_ref)* vec_ref
        vector_ortho *= numpy.dot(self.update_vector - numpy.dot(self.update_vector, vec_ref)* vec_ref, vector_ortho) 
        vector_ortho /= sum(vector_ortho ** 2.0) ** 0.5  
        return vector_ortho

    def naivefull_vector_ortho(self, vec_ref):
        """ Pick a random vector in the hyperplan"""
        vector_ortho = numpy.random.normal(0.0, 1.0, self.TargetDistribution.N_DIM)
        vector_ortho -= numpy.dot(vector_ortho, vec_ref)* vec_ref
        vector_ortho /= sum(vector_ortho ** 2.0) ** 0.5  
        return vector_ortho

    def posswitch_vector_ortho(self, vec_ref):
        """ Switch the components along the Gramschmidt-obtained vectors. Align to former direction"""
        vector_ortho = self.update_vector.copy()
        vector_ortho -= numpy.dot(vector_ortho, vec_ref) * vec_ref 
        G_vec_norm = self.get_GramSchmidt_vector(vec_ref)
        G_vector_scalar = numpy.dot(G_vec_norm, vector_ortho)
        vector_ortho += numpy.dot(G_vector_scalar, G_vec_norm[::-1] -  G_vec_norm)
        vector_ortho *= numpy.dot(self.update_vector - numpy.dot(self.update_vector, vec_ref)* vec_ref, vector_ortho) 
        vector_ortho /= sum(vector_ortho ** 2.0) ** 0.5  
        return vector_ortho
    
    def posperpswitch_vector_ortho(self, vec_ref):
        """ Orthogonally switch the components along the Gramschmidt-obtained vectors. Align to former direction"""
        vector_ortho = self.update_vector.copy()
        vector_ortho -= numpy.dot(vector_ortho, vec_ref) * vec_ref 
        G_vec_norm = self.get_GramSchmidt_vector(vec_ref)
        G_vector_scalar = numpy.dot(G_vec_norm, vector_ortho)
        vector_ortho += numpy.dot(G_vector_scalar[0], G_vec_norm[1] -  G_vec_norm[0])
        vector_ortho += numpy.dot(G_vector_scalar[1], -G_vec_norm[0] -  G_vec_norm[1])
        vector_ortho *= numpy.dot(self.update_vector - numpy.dot(self.update_vector, vec_ref)* vec_ref, vector_ortho) 
        vector_ortho /= sum(vector_ortho ** 2.0) ** 0.5  
        return vector_ortho

    def ranp_vector_ortho(self, vec_ref):
        """ Apply a random rotation in the subspace of dimension self.dim_refresh in the hyperplan"""
        vector_ortho = self.update_vector.copy()
        vector_ortho -= numpy.dot(vector_ortho, vec_ref) * vec_ref 
        G_vec_norm = self.get_GramSchmidt_vector_highdim(vec_ref)
        scalar = numpy.array([numpy.dot(G_vec_norm[i], vector_ortho) for i in range(self.dim_refresh)])
        normold = sum(scalar ** 2.0) ** 0.5
        subvector = numpy.random.normal(0.0, 1.0, self.dim_refresh)
        subvector *= normold / sum(subvector ** 2.0) ** 0.5 
        for i in range(self.dim_refresh):      
            vector_ortho -= scalar[i] * G_vec_norm[i]
            vector_ortho += subvector[i] * G_vec_norm[i]
        vector_ortho *= numpy.dot(self.update_vector - numpy.dot(self.update_vector, vec_ref)* vec_ref , vector_ortho) 
        vector_ortho /= sum(vector_ortho ** 2.0) ** 0.5 
        return vector_ortho

    def theta2_vector_ortho(self, vec_ref):
        """ Apply a rotation of angle self.THETA in the subspace of dimension self.dim_refresh in the hyperplan"""
        vector_ortho = self.update_vector.copy()
        vector_ortho -= numpy.dot(vector_ortho, vec_ref) * vec_ref 
        G_vec_norm = self.get_GramSchmidt_vector(vec_ref)
        scalar1 = numpy.dot(G_vec_norm[0], vector_ortho)
        scalar2 = numpy.dot(G_vec_norm[1], vector_ortho)
        vector_ortho -= scalar1 * G_vec_norm[0] + scalar2 * G_vec_norm[1]
        vector_ortho += scalar1 * (G_vec_norm[0] * math.cos(self.THETA) + G_vec_norm[1] * math.sin(self.THETA))
        vector_ortho += scalar2 * (G_vec_norm[0] * math.sin(self.THETA) - G_vec_norm[1] * math.cos(self.THETA))
        vector_ortho *= numpy.dot(self.update_vector - numpy.dot(self.update_vector, vec_ref)* vec_ref, vector_ortho) 
        vector_ortho /= sum(vector_ortho ** 2.0) ** 0.5  
        return vector_ortho


    def direct_pick(self, vec_ref):
        """ Directly pick a new component along vec_ref"""
        self.update_vector_store=self.update_vector.copy()  
        grad_component_old = numpy.dot(self.update_vector, vec_ref)
        grad_component_new = -(1.0 - (random.uniform(0.0, 1.0) ** (1.0 / (self.TargetDistribution.N_DIM - 1.0))) ** 2.0) ** 0.5
        self.update_vector -= grad_component_old * vec_ref 
        self.update_vector /= sum(self.update_vector ** 2.0) ** 0.5
        self.update_vector = (1-grad_component_new ** 2.0) ** 0.5 * self.update_vector + grad_component_new * vec_ref 

    def isometro_pick(self, vec_ref):
        """ Hasting-Metropolis update of a new component along vec_ref. Gaussian proposal."""
        self.update_vector_store=self.update_vector.copy() 
        grad_component_old = numpy.dot(self.update_vector, vec_ref)               
        ran_vec = numpy.random.normal(0,1,self.TargetDistribution.N_DIM)
        ran_vec /= sum(ran_vec**2.0)**0.5
        grad_component_new = -abs(ran_vec[0])
        if not random.uniform(0.0,1.0)<= min(1, abs(grad_component_new/grad_component_old)):
            grad_component_new = -grad_component_old
        else:
            self.rate += 1.0
        self.update_vector -= grad_component_old * vec_ref 
        self.update_vector /= sum(self.update_vector ** 2.0) ** 0.5
        self.update_vector = (1-grad_component_new ** 2.0) ** 0.5 * self.update_vector + grad_component_new * vec_ref 

    def stepmetro_pick(self, vec_ref):
        """ Hasting-Metropolis update of a new component along vec_ref. Uniform proposal."""
        self.update_vector_store=self.update_vector.copy() 
        grad_component_old = numpy.dot(self.update_vector, vec_ref)  
        grad_component_new = grad_component_old + (random.uniform(0,1.0)-1./2)
        grad_component_new = -(grad_component_new%1)
        pacc = abs(grad_component_new) / grad_component_old
        pacc *= (1.0-grad_component_new**2.0)**((self.TargetDistribution.N_DIM -3)/2.0)
        pacc /= (1.0-grad_component_old**2.0)**((self.TargetDistribution.N_DIM -3)/2.0)
        if not random.uniform(0.0,1.0)<= min(1, pacc):
            grad_component_new = -grad_component_old
        else:
            self.rate += 1.0
        self.update_vector -= grad_component_old * vec_ref 
        self.update_vector /= sum(self.update_vector ** 2.0) ** 0.5
        self.update_vector = (1-grad_component_new ** 2.0) ** 0.5 * self.update_vector + grad_component_new * vec_ref 
        
    def reflection_pick(self, vec_ref): 
        """ Reflect vector along vec_ref"""
        self.update_vector_store=self.update_vector.copy()
        self.update_vector -= 2.0 * numpy.dot(self.update_vector, vec_ref) * vec_ref

    def straight_pick(self, factor): 
        """ Invert vector"""
        if factor is None:
            self.update_vector = -self.update_vector
        else:    
            self.update_vector[factor] = -self.update_vector[factor]

    def output_configuration(self, new_distance):
        """Run a PDMC until a sample is ready """
        chain_over = False
        while new_distance > self.running_output_distance :
            self.Run.C = self.TargetDistribution.do_lift(self.Run.C, self.running_output_distance, self.update_vector)
            self.C_Store += [self.Run.C.copy()]
            new_distance -= self.running_output_distance
            self.running_output_distance = self.Run.OUTPUT_DISTANCE
            chain_over = True
        self.running_output_distance -= new_distance  
        return chain_over, new_distance

    def Event_RefreshAtConfig(self):
        """Run a PDMC with refreshment outside events """
        new_distance = self.TargetDistribution.get_next_event(self.Run.C, self.update_vector)
        self.running_distance_refresh -= new_distance 
        new_distance = min(new_distance, self.running_distance_refresh + new_distance)
        chain_over, new_distance = self.output_configuration(new_distance) 
        self.Run.C = self.TargetDistribution.do_lift(self.Run.C, new_distance, self.update_vector)
        grad_E_norm = self.get_gradE_norm() 
        if self.running_distance_refresh < 0.0:
            self.running_distance_refresh = self.Run.distance_refresh 
            self.refresh_vector(vec_ref=grad_E_norm)            
        else:   
            self.new_event_vector_pick(grad_E_norm)
        return chain_over

    def Event_RefreshAtEvent(self):
        """Run a PDMC with refreshment at events """
        new_distance = self.TargetDistribution.get_next_event(self.Run.C, self.update_vector)
        self.running_distance_refresh -= new_distance 
        chain_over, new_distance = self.output_configuration(new_distance)  
        self.Run.C = self.TargetDistribution.do_lift(self.Run.C, new_distance, self.update_vector)
        grad_E_norm = self.get_gradE_norm()
        if self.running_distance_refresh < 0.0:
            self.running_distance_refresh += self.Run.distance_refresh
            self.refresh_vector(vec_ref=grad_E_norm)  
        self.new_event_vector_pick(grad_E_norm)
        return chain_over 

    def Event_RefreshAtEvent_Proba(self):
        """Run a PDMC with refreshment at events with a certain probability"""
        new_distance = self.TargetDistribution.get_next_event(self.Run.C, self.update_vector)
        chain_over, new_distance = self.output_configuration(new_distance)  
        self.Run.C = self.TargetDistribution.do_lift(self.Run.C, new_distance, self.update_vector)
        grad_E_norm = self.get_gradE()
        if random.uniform(0.0, 1.0) < self.p_switch:
            self.refresh_vector(vec_ref=grad_E_norm)  
        self.new_event_vector_pick(grad_E_norm)
        return chain_over   

    def Event_RefreshAtAllEvent(self):
        """Run a PDMC with refreshment at all events """
        new_distance = self.TargetDistribution.get_next_event(self.Run.C, self.update_vector)
        chain_over, new_distance = self.output_configuration(new_distance)  
        self.Run.C = self.TargetDistribution.do_lift(self.Run.C, new_distance, self.update_vector)
        grad_E_norm = self.get_gradE_norm()
        self.refresh_vector(vec_ref=grad_E_norm) 
        self.new_event_vector_pick(grad_E_norm)
        return chain_over  

    def Event_NoRefresh(self):
        """Run a PDMC. No refreshment"""
        new_distance = self.TargetDistribution.get_next_event(self.Run.C, self.update_vector)
        chain_over, new_distance = self.output_configuration(new_distance)  
        self.Run.C = self.TargetDistribution.do_lift(self.Run.C, new_distance, self.update_vector)
        grad_E_norm = self.get_gradE_norm()
        self.new_event_vector_pick(grad_E_norm)
        return chain_over     

##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~###
def main(): 
    """If called, initialize the parser and the sampling experiment with user's arguments.
     Run the sampler.
    """    
    print 'Initialization...'
    ParametersInitialization = ParserMCGeneral()
    SamplingExperiment = RunMCGeneral(ParametersInitialization.parameters_sampling)
    if 'Event' in SamplingExperiment.SCHEME:
        chain = ECMCMethodLibrary(SamplingExperiment)
    elif 'Metropolis' in SamplingExperiment.SCHEME:
        chain = MCMCMethodLibrary(SamplingExperiment)  
    elif 'HMC' in SamplingExperiment.SCHEME:
        chain = HMCMethodLibrary(SamplingExperiment)  
    elif 'OptimizedIrr' in SamplingExperiment.SCHEME:
        chain = OptimizedIrrMethodLibrary(SamplingExperiment)        
    chain.N_new_sample()
    return
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~###
main()
