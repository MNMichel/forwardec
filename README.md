# Forward event-chain algorithmic class #

## Description ##

Codes for numerical experiments of [https://arxiv.org/abs/1702.08397](https://arxiv.org/abs/1702.08397): Implementation of MCMC methods based on piecewise deterministic Markov processes and decomposition of the Markov kernel between the gradient (K_para) and the orthogonal plan (K_perp), leading to stochastic direction changes at jump times. Comparison to [EC](https://aip.scitation.org/doi/10.1063/1.4863991), [BPS](https://www.tandfonline.com/doi/abs/10.1080/01621459.2017.1294075), [ZZ](https://arxiv.org/abs/1607.03188) and [HMC](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.446.9306&rep=rep1&type=pdf) schemes.

## To  clone the repository ##

`git clone  git@bitbucket.org:MNMichel/forwardec.git`

## Usage policy ##

Please cite the following: Michel, Durmus et Sénécal, arXiv preprint, arXiv:1702.08397

## To get the code running ##

### Requirements ###

The code is written in Python 2.7 and requires the following modules
to be installed:

* sys, os, time, pickle, argparse, importlib
* math, random
* numpy  
* scipy 

### In-built help ###

`python -u GeneralMCMCRunner.py -h` will give help on the parser arguments.

`python -u GeneralMCMCRunner.py -h -Library Gaussian -DistributionClass GaussianLogLinearVar` will also include the help on the specified Library (= name of the file in DistributionClassLibrary folder) and Target distribution (= name of the class in the Library file) (replace `Gaussian` by Library of interest and `GaussianLogLinearVar` by DistributionClass of interest).

### For Anisotropic Gaussian experiments ###

#### For Forward and BPS schemes 
    python -u GeneralMCMCRunner.py  -SCHEME $scheme  -TOTAL_NUMBER_SAMPLE 1000 -OUTPUT_DISTANCE 500 -OBS x_E_norm -STORE . -Library Gaussian -DistributionClass GaussianLogLinearVar -NPY -N_DIM 100 -VARIANCE_ARG 6 -CONTINUE >> 'AnisotropicGaussian.out'  

##### Schemes to consider (specify `$scheme`)
*     `Event_RefreshAtAllEvent__DirectPick__PosSwitchOrtho` (Forward All Ref)  
*     `Event_RefreshAtEvent__DirectPick__PosSwitchOrtho  -REFRESH 500` (Forward Ref)  
*     `Event_RefreshAtConfig__DirectPick__FullRotate  -REFRESH 500` (Forward Full Ref)  
*     `Event_RefreshAtConfig__ReflectionPick__FullRotate  -REFRESH 500` (BPS = reflection pick with full refresh)  
* In supplement:  
    -  `Event_RefreshAtConfig__ReflectionPick__PosSwitchOrtho  -REFRESH 500` (reflection pick with pos orthogonal switch)  
    - `Event_RefreshAtConfig__ReflectionPick__NaiveSwitchOrtho  -REFRESH 500` (reflection pick with naive orthogonal switch)  
    - `Event_RefreshAtEvent__DirectPick__PosFullOrtho  -REFRESH 500` (pos full orthogonal refresh and direct pick)  
    - `Event_RefreshAtEvent__DirectPick__NaiveFullOrtho  -REFRESH 500` (naive full orthogonal refresh and direct pick)  
    - `Event_RefreshAtEvent__DirectPick__RanPOrtho  -REFRESH 500` (ran-p-orthogonal refresh and direct pick -- specify p by adding option `-DIM_REFRESH $p`)  
    - `Event_RefreshAtEvent__DirectPick__NaiveSwitchOrtho  -REFRESH 500` (naive orthogonal switch and direct pick)  
    - `Event_RefreshAtEvent__DirectPick__PosPerpSwitchOrtho  -REFRESH 500` (pos perp orthogonal switch and direct pick)  
    - `Event_RefreshAtEvent__DirectPick__FixThetaOrtho  -REFRESH 500` (theta-2-orthogonal rotation switch and direct pick  -- specify theta by adding option `-THETA $theta` (in unit of pi))  
    - `Event_RefreshAtEvent__IsoMetroPick__PosSwitchOrtho  -REFRESH 500` (orthogonal switch and Metropolis pick with Gaussian noise)  
    - `Event_RefreshAtEvent__StepMetroPick__PosSwitchOrtho  -REFRESH 500` (orthogonal switch and Metropolis pick with uniform noise)  


#### For ZZ and optimized EC schemes 
    python -u GeneralMCMCRunner.py  -SCHEME $scheme  -TOTAL_NUMBER_SAMPLE 1000 -OUTPUT_DISTANCE 100 -OBS x_E_norm -STORE . -Library Gaussian -DistributionClass GaussianLogLinearVar -NPY -N_DIM 100 -VARIANCE_ARG 6 -FACTOR -CONTINUE >> 'AnisotropicGaussian.out'  

##### Schemes to consider  (specify `$scheme`)
* `OptimizedIrr` (Optimized EC)  
* `Event_NoRefresh__StraightPick` (ZZ fit Metric)  
* `Event_NoRefresh__IllStraightPick -ILL` (ZZ)  


#### For HMC scheme 
    python -u GeneralMCMCRunner.py  -SCHEME HMC  -TOTAL_NUMBER_SAMPLE 1000 -OUTPUT_DISTANCE 30 -OBS x_E_norm -STORE . -Library Gaussian -DistributionClass GaussianLogLinearVar -NPY -N_DIM 100 -VARIANCE_ARG 6 -CONTINUE >> 'AnisotropicGaussian.out'  

------------------------------------------------------------------------------------------------------
### For Poisson-Gaussian Markov random field experiments ###

    python -u GeneralMCMCRunner.py  -SCHEME $scheme  -TOTAL_NUMBER_SAMPLE 1000 -OUTPUT_DISTANCE 1 -OBS x_E_norm -STORE . -Library PoissonGaussModel -DistributionClass GaussianMixture -NPY -N_DIM 16 -VARIANCE_ARG 3 -CONTINUE >> 'PoissonGauss.out'  

##### Schemes to consider (specify `$scheme`)
*     `Event_RefreshAtAllEvent__DirectPick__PosSwitchOrtho` (Forward All Ref)  
*     `Event_RefreshAtEvent__DirectPick__PosSwitchOrtho -REFRESH 5` (Forward Ref)  
*     `Event_RefreshAtConfig__DirectPick__FullRotate -REFRESH 2` (Forward Full Ref)  
*     `HMC` (HMC)

------------------------------------------------------------------------------------------------------

### For Gaussian mixtures ###

    python -u GeneralMCMCRunner.py -SCHEME $scheme -TOTAL_NUMBER_SAMPLE 1000 -OUTPUT_DISTANCE 10 -OBS x_norm -STORE . -Library GaussianPrior -DistributionClass RanNGaussianND -N_DIM 4 -N_GAUSS 5 -NPY -SEED 1 -REFRESH 100 -INIT_EQ >> 'GaussianMix.out'  

Option `-INIT_EQ`: to start from random position drawn from target distribution  

#### Schemes to consider  (specify `$scheme`)
* `Event_RefreshAtAllEvent__DirectPick__PosSwitchOrtho` (Forward All Ref)  
* `Event_RefreshAtEvent__DirectPick__PosSwitchOrtho` (Forward Ref)  
* `Event_NoRefresh__DirectPick` (Forward No Ref)  
* `Event_RefreshAtConfig__DirectPick__FullRotate` (Forward Full Ref)  
* `Event_RefreshAtConfig__ReflectionPick__FullRotate` (BPS)  
* `Event_NoRefresh__ReflectionPick` (BPS No Ref)  
* In supplement:  
    - `Event_RefreshAtEvent__IsoMetroPick__PosSwitchOrtho` (orthogonal switch and Metropolis pick with Gaussian noise)  
    - `Event_RefreshAtEvent__StepMetroPick__PosSwitchOrtho` (orthogonal switch and Metropolis pick with uniform noise)  
    - `Event_NoRefresh__IsoMetroPick` (Metropolis pick with Gaussian noise)  
    - `Event_NoRefresh__StepMetroPick` (Metropolis pick with uniform noise)  

------------------------------------------------------------------------------------------------------
### For Logistic regression experiments ###

#### For Musk dataset
    python -u GeneralMCMCRunner.py  -SCHEME $scheme  -TOTAL_NUMBER_SAMPLE 1000 -OUTPUT_DISTANCE 100 -REFRESH 100 -OBS x_L_varw -STORE . -Library LogisticReg -DistributionClass GeneralLogRegWithBias -NPY -DATASET Musk -VARIANCE_PRIOR 1000 -CONTINUE >>'LogRegMusk.out'  

#### For German Credit dataset 
    python -u GeneralMCMCRunner.py  -SCHEME $scheme  -TOTAL_NUMBER_SAMPLE 1000 -REFRESH 0.1 -OUTPUT_DISTANCE 0.1 -OBS x_L_varw -STORE . -Library LogisticReg -DistributionClass GeneralLogRegWithBias -NPY -DATASET GermanCredit -VARIANCE_PRIOR 1000 -CONTINUE >>'LogRegGerman.out'  

#### Schemes to consider  (specify `$scheme`)
* `Event_RefreshAtAllEvent__DirectPick__PosSwitchOrtho` (Forward All Ref)  
* `Event_RefreshAtEvent__DirectPick__PosSwitchOrtho` (Forward Ref)  
* `Event_NoRefresh__DirectPick` (Forward No Ref)  
* `Event_RefreshAtConfig__DirectPick__FullRotate` (Forward Full Ref)  
* `Event_RefreshAtConfig__ReflectionPick__FullRotate` (BPS)  
* `Event_NoRefresh__ReflectionPick` (BPS No Ref)  
